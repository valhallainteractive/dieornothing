﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    [SerializeField] private GameObject item;
    // Start is called before the first frame update
    void Start()
    {
        ItemOff();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Engrais"))
        {
            Destroy(collision.gameObject);
            item.gameObject.SetActive(true);
            GameManager.instance.PlaySuccess();
            StartCoroutine(Video());
        }
    }
    private void ItemOff()
    {
         item.gameObject.SetActive(false);
    }
    IEnumerator Video()
    {
        yield return new WaitForSeconds(0.7f);
        GameManager.instance.PlayVideo();
        GameManager.instance.KillPlayer();
        Destroy(gameObject);
    }
}   
