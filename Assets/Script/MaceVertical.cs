﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaceVertical : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float newSpeed;
    [SerializeField] private float maxTranslate;
    float yMin;
    float yMax;

    // Start is called before the first frame update
    void Start()
    {
        yMin = transform.position.y - maxTranslate;
        yMax = transform.position.y + maxTranslate;
    }

    // Update is called once per frame
    void Update()
    {
        TranslateVertical();
    }

    private void TranslateVertical()
    {

        transform.Translate(Vector2.up * speed * newSpeed * Time.deltaTime);

        if (transform.position.y < yMin)
        {
            newSpeed = 1;
        }
        else if (transform.position.y > yMax)
        {
            newSpeed = -1;
        }
    }
}
