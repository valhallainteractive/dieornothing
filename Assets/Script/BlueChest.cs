﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BlueChest : MonoBehaviour
{
    [SerializeField] private string enigme;
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject[] objectTranparence;
    private TextMeshProUGUI placeEnigme;
    // Start is called before the first frame update
    void Start()
    {
        placeEnigme = transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>();
        
        anim.SetBool("OpenChest", false);
        HideObject();
    }

    // Update is called once per frame
    void Update()
    {
        HideCanvas();
        TextEnigme();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (Input.GetKey(KeyCode.E) && anim.GetBool("OpenChest") == false)
            {
                anim.SetBool("OpenChest", true);
                AppearObject();
            }
        }
    }
    private void HideObject()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        
        foreach (GameObject item in objectTranparence)
        {
            item.SetActive(false);
        }
    }
    private void AppearObject()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        foreach (GameObject item in objectTranparence)
        {
            item.SetActive(true);
        }
    }
    private void HideCanvas()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    private void TextEnigme()
    {
        placeEnigme.SetText(enigme);
    }
}
