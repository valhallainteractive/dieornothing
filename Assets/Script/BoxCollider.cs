﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCollider : MonoBehaviour
{
    [SerializeField] private GameObject item;
    // Start is called before the first frame update
    void Start()
    {
        item.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            GameManager.instance.PlaySuccess();
            item.gameObject.SetActive(true);
            Destroy(this);
        }
    }
}
