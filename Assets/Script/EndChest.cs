﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndChest : MonoBehaviour
{
    [SerializeField] private Animator animChest;
    // Start is called before the first frame update
    void Start()
    {
        animChest.SetBool("Open", false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (Input.GetKey(KeyCode.E) &&  animChest.GetBool("Open") == false)
            {
                GameManager.instance.PlaySuccess();
                animChest.SetBool("Open", true);
                StartCoroutine(NextLevel());
            }
        }
    }
    IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }
}
