﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformChest : MonoBehaviour
{
    [SerializeField] private GameObject platform;
    [SerializeField] private Animator anim;
    [SerializeField] private AudioClip clip;
    // Start is called before the first frame update
    void Start()
    {
        anim.SetBool("Open", false);
        platform.gameObject.SetActive(false);
    }
    
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (Input.GetKey(KeyCode.E) && anim.GetBool("Open") == false)
            {
                anim.SetBool("Open", true);
                platform.gameObject.SetActive(true);
                GameManager.instance.PlaySuccess();
                GameManager.instance.SetCheckPoint(transform);
            }
        }
    }
}
