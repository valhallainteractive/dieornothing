﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockPlatform : MonoBehaviour
{
    [SerializeField] private GameObject[] platforms;
    [SerializeField] private bool destroy;

    private void Start()
    {
        HidePlatform();
    }
    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            GameManager.instance.PlaySuccess();
            StartCoroutine(SpawnPlatforms());
        }
    }

    private IEnumerator SpawnPlatforms()
    {
        yield return new WaitForSeconds(1f);
        
        foreach (GameObject platform in platforms)
        {
            platform.SetActive(true);
        }
            
        if(destroy)
            Destroy(gameObject);
        else
            Destroy(this);    
    }
    private void HidePlatform()
    {
        foreach (GameObject platform in platforms)
        {
            platform.SetActive(false);
        }
    }
}
