﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Water : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            GameManager.instance.PlayVideoEnd();
            StartCoroutine(VideoEnd());
        }
    }
    IEnumerator VideoEnd()
    {
         yield return new WaitForSeconds((float)GameManager.instance.endPlayer.clip.length);
         SceneManager.LoadScene("End");
    }
}
