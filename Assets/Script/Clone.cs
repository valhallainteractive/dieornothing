﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clone : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Animator anim;
    [SerializeField] private float jumpForce;
    [SerializeField] private LayerMask ground;
    [SerializeField] private GameObject deadEffect;
    private Rigidbody2D jump;
    private SpriteRenderer spritePlayer;
    [SerializeField]private Collider2D feetCollider;
    private int airJump = 1;
    private Camera defaultCamera;
    [SerializeField] private Camera cloneCamera;

    // Start is called before the first frame update
    void Start()
    {
        defaultCamera = Camera.main;
        defaultCamera.gameObject.SetActive(false);
        cloneCamera.gameObject.SetActive(true);
        
        jump = GetComponent<Rigidbody2D>();
        spritePlayer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        RunPlayer();
        JumpPlayer();
    }
    private void RunPlayer()
    {
        float horizontal = -Input.GetAxis("Horizontal");
        transform.Translate(Vector2.right * speed * Time.deltaTime * horizontal);
        anim.SetBool("Run", horizontal != 0);
        if (horizontal < 0)
        {
            spritePlayer.flipX = true;
        }
        else
        {
            spritePlayer.flipX = false;
        }

    }
    private void JumpPlayer()
    {
        if(feetCollider.IsTouchingLayers(ground))
        {
            anim.SetBool("Grounded", true);
            airJump = 1;
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jump.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                anim.SetTrigger("Jump");
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space) && airJump > 0)
            {
                airJump--;
                jump.velocity = new Vector2(jump.velocity.x,0);
                jump.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                anim.SetTrigger("Jump");
            }
            
            anim.SetBool("Grounded", false);
        }
    }

    public void Dead()
    {
        GameManager.instance.PlaySuccess();
       Destroy(gameObject);
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            defaultCamera.gameObject.SetActive(true);
            cloneCamera.gameObject.SetActive(false);
            Dead();
        }
    }
}
