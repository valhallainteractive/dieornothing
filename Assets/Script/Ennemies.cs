﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemies : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float newSpeed;
    [SerializeField] private float maxTranslate;
    float xMin; 
    float xMax;

    // Start is called before the first frame update
    void Start()
    {
         xMin = transform.position.x - maxTranslate;
         xMax = transform.position.x + maxTranslate;
    }

    // Update is called once per frame
    void Update()
    {
        TranslateEnnemies();
    }

    private void TranslateEnnemies()
    {

        transform.Translate(Vector2.right *speed*newSpeed * Time.deltaTime);

        if (transform.position.x < xMin)
        {
            newSpeed = 1;
        }
        else if (transform.position.x > xMax)
        {
            newSpeed = -1;
        }
    }
}
