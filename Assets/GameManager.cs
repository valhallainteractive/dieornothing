﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Video;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] private Transform checkpoint;
    [SerializeField] private GameObject player;
    private AudioSource audio;
    [SerializeField] private AudioClip success;
    [SerializeField] private AudioClip deadSound;
    [SerializeField] private GameObject video;
    [SerializeField] private GameObject videoEnd;
    [SerializeField] private VideoPlayer treePlayer;
    public  VideoPlayer endPlayer;
    private void Awake()
    {
        instance = this;
        audio = GetComponent<AudioSource>();
    }
    
    public void ReSpawn()
    {
        player.GetComponent<SpriteRenderer>().enabled = true;
        player.transform.position = checkpoint.position;
    }

    public void SetCheckPoint(Transform newCheckpoint)
    {
        checkpoint = newCheckpoint;
    }

    public void PlaySuccess()
    {
        audio.PlayOneShot(success);
    }

    public void PlayDead()
    {
        audio.PlayOneShot(deadSound);
    }

    public void PlayVideo()
    {
        video.SetActive(true);   
        treePlayer.Play();
        StartCoroutine(StopVideo());
    }

    public IEnumerator StopVideo()
    {
        yield return new WaitForSeconds((float)GetComponent<VideoPlayer>().clip.length);
        treePlayer.Stop();
        video.SetActive(false);  
    }

    public void PlayVideoEnd()
    {
        videoEnd.SetActive(true);
        endPlayer.Play();
        StartCoroutine(StopVideoEnd());
    }
    public IEnumerator StopVideoEnd()
    {
        yield return new WaitForSeconds((float)GetComponent<VideoPlayer>().clip.length);
        endPlayer.Stop();
        videoEnd.SetActive(false);
    }
    public void KillPlayer()
    {
        StartCoroutine(player.GetComponent<Player>().Dead());
    }
}
