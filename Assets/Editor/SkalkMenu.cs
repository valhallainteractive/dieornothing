﻿using UnityEditor;
using UnityEditor.SceneManagement;

public class SkalkMenu
{
    [MenuItem("Skalkevai/Application/Play Game _F5")]
    private static void PlayGame()
    {
        EditorApplication.EnterPlaymode();
    }
    
    [MenuItem("Skalkevai/Application/Pause Game _F6")]
    private static void PauseGame()
    {
        EditorApplication.isPaused = !EditorApplication.isPaused;
    }
    
    [MenuItem("Skalkevai/Application/Stop Game _F7")]
    private static void StopGame()
    {
        EditorApplication.ExitPlaymode();
    }
}
